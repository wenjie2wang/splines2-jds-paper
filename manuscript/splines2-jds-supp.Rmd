---
title: >-
  Supplementary Materials to
  "Shape-Restricted Regression Splines with \proglang{R} Package \pkg{splines2}"
short-title: Supplementary Materials to Regression Splines with \pkg{splines2}
author:
  - name: "Wenjie Wang"
    marker: 1
  - name: "Jun Yan"
    marker: 2
short-authors: Wang, W. and Yan J.
affiliation:
  - name: >
      Eli Lilly and Company,
      Indianapolis, Indiana, USA.
    marker: 1
  - name: >
      Department of Statistics,
      University of Connecticut,
      Storrs, Connecticut, USA.
    marker: 2
classoption:
  - letterpaper
  - inpress
  - supp
  - linenumber
year: 2021
month: July
bibliography: splines2-jds.bib
output:
  jds.rmd::pdf_article:
    cls: jds
    includes:
      in_header: "preamble.tex"
---


```{r setup, echo = FALSE, warning = FALSE, message = ""}
knitr::opts_knit$set(global.par = TRUE)
knitr::opts_chunk$set(echo = TRUE, warning = FALSE, cache = FALSE,
                      fig.width = 6, fig.height = 3.5, dpi = 300,
                      fig.align = "center", out.width = "\\linewidth")
library(splines2)

## helper function for matplot
plot_mat <- function(x, mat, ...) {
    matplot(x, mat, type = "l", ...)
    if (! is.null(knots(mat)))
        abline(v = knots(mat), lty = 2, col = "gray")
}
```

```{r set-par, echo = FALSE}
library(graphics)
par(mar = c(2.5, 2.5, 0.5, 0.5), mgp = c(1.5, 0.5, 0),
    cex.lab = 1, cex.axis = 1, cex = 0.8)
```

# Polynomial/Spline Basis Functions

In this section, we introduce the polynomial and spline basis functions that are
implemented in the package \pkg{splines2} but not covered in the main text.

## Generalized Bernstein Polynomials

Bernstein polynomials of degree $k$ correspond to the polynomial terms
from the binomial expansion of $1=[u+(1-u)]^k$.
The $i$th Bernstein polynomial basis
denoted by $\tilde{G}_{i,k}(u)$ are defined for $0\le u\le 1$ as
\begin{align}\label{eq:bern:1}
\tilde{G}_{i,k}(u)=\binom{k}{i} u^i (1-u)^{k-i},~i\in\{0,\ldots,k\}.
\end{align}
For a given boundary $[L,U]$ and $L \le x \le U$,
the generalized Bernstein polynomials are defined
by replacing $u$ with $(x-L)/(U-L)$ in \eqref{eq:bern:1},
\begin{align}\label{eq:bern:2}
G_{i,k}(x)=\frac{1}{(U-L)^k} \binom{k}{i}
(x-L)^i (U-x)^{k-i},~i\in\{0,\ldots,k\},
\end{align}
which can also be defined through a recursive manner
[see @prautzsch2002bezier, Chapter 2] as follows:
\begin{align*}
G_{i,k}(x) = \frac{x-L}{U-L} G_{i-1,k-1}(x) +
\frac{U-x}{U-L} G_{i,k-1}(x),~i\in\{0,\ldots,k\},
\end{align*}
where $G_{0,0}(x) = 1$ and
$G_{-1,k-1}(x)=G_{k,k-1}=0$ for $k\in\{1,2,\ldots\}$.
In fact, the generalized Bernstein polynomials of degree $k$
are equivalent to the B-splines of a same degree with no internal knots.
See Section \@ref(sec:bs) for a brief introduction to B-splines.
Therefore, Bernstein polynomials and B-splines
share several common properties.
For example, we have
$G_{i,k}(x) \ge 0$, $\sum_{i=0}^{k} G_{i,k}(x)=1$,
$G_{i,k}(x-L)=G_{k-i,k}(U-x)$, and
$G_{i,k}(L)=G_{k-i,k}(U)=\mathbbm{1}(i=0)$, $\forall i\in\{0,\ldots,k\}$.

<!-- derivatives -->
The first derivatives of the generalized Bernstein polynomials
can be derived directly from \eqref{eq:bern:2} or given recursively by
\begin{align*}
\dv{x} G_{i,k}(x) = \frac{k}{U-L}\left(
G_{i-1,k-1}(x) - G_{i,k-1}(x)\right),
\end{align*}
which makes it easier to obtain the derivatives of higher order.
<!-- integrals -->
Similarly, the integrals of the generalized Bernstein polynomials
can be defined recursively by
\begin{align*}
\int_{L}^x G_{i,k}(t)dt = \frac{U-L}{k+1} \sum_{l=i+1}^{k+1} G_{l,k+1}(x).
\end{align*}

<!-- visualization -->
Figure\ \@ref(fig:plot-bernstein) visualizes the generalized
Bernstein polynomial basis functions of degree 3, their first derivatives,
and integrals, respectively, which can be obtained by the function
`bernsteinPoly()` of the package \pkg{splines2} as follows:

```{r bernstein-poly-examples}
x <- seq.int(- 1, 2, 0.01) # set x from -1 to 2
## generalized cubic Bernstein polynomials, their derivatives, and integrals
bp_mat <- bernsteinPoly(x, intercept = TRUE)                   # basis functions
dbp_mat <- bernsteinPoly(x, intercept = TRUE, derivs = 1)      # 1st derivatives
ibp_mat <- bernsteinPoly(x, intercept = TRUE, integral = TRUE) # integrals
```

The cubic basis functions were produced by default.
For the first derivatives, we specified the argument `derivs = 1` of the
function `bernsteinPoly()`.
Alternatively, we can utilize the function `deriv()` to obtain the first
derivatives with ease (e.g., `deriv(bp_mat)`).
The integrals were returned when we specified `integral = TRUE`
(and `derivs = 0` by default).

(ref:cap-plot-bernstein) Generalized Cubic Bernstein Polynomial basis functions and their first derivatives and integrals.

```{r plot-bernstein, echo = FALSE, fig.width = 6, fig.height = 2, fig.cap = "(ref:cap-plot-bernstein)"}
par(mfrow = c(1, 3))
plot_mat(x, bp_mat, ylab = "Generalized Bernstein Polynomials")
plot_mat(x, dbp_mat, ylab = "First Derivatives")
plot_mat(x, ibp_mat, ylab = "Integrals")
```

## B-Splines {#sec:bs}

B-splines or basic splines are widely used tools in numerical analysis and have
close connections to other spline basis functions introduced in this article and
the main text.
For a given $k\in\{1,2,\ldots\}$,
the $i$th B-spline basis of degree $k$ (or order $d=k+1$)
denoted by $B_{i,k}(x)$ based on the simple knot sequence $\bm{s}_k$
can be defined by the following Cox--de Boor recursive
formula [@cox1972numerical;@boor1972calculating]:
\begin{align}\label{eq:bs:1}
B_{i,k}(x\mid \bm{s}_k) =
\left(\frac{x-t_i}{t_{i+k}-t_i}\right) B_{i,k-1}(x\mid \bm{s}_k) +
\left(\frac{t_{i+k+1}-x}{t_{i+k+1}-t_{i+1}}\right)
B_{i+1,k-1}(x\mid \bm{s}_k),
\end{align}
with
\begin{align}\label{eq:bs:2}
B_{l,0}(x\mid \bm{s}_k) = \begin{cases}
1, & t_l \le x < t_{l+1}\\
0, & \mathrm{otherwise}
\end{cases},~l \in\{1,\ldots,d+p-1\},
\end{align}
where
$L\le x<U$, $p=m+d$ represents the degrees of freedom,
and $i\in\{1,\ldots,p\}$.
The recursive definition implicitly follows the convention that
$B_{i,k-1}(x\mid \bm{s}_k)=B_{i,k-1}(x\mid \bm{s}_k)/(t_{i+k}-t_{i})=0$
if $t_{i+k}=t_{i}$.
<!-- one computational note -->
In practice, we may let $B_{i,k}(U\mid \bm{s}_k)$
take the limit from the left
so that each basis is defined for all $x\in\{x\mid L\le x\le U\}$.


<!-- how to apply the recursive formula -->
Notice that the simple knot sequence $\bm{s}_k$
that depends on the boundary knots,
the internal knots, and the degree $k$ is fixed
when we apply the recursive formula \eqref{eq:bs:1}.
For instance, the simple knot sequence $\bm{s}_2$ for the quadratic spline basis
functions satisfies
$t_1=t_2=t_3=L<t_4<\cdots<t_{m+3}<U=t_{m+4}=t_{m+5}=t_{m+6}$,
where $t_{j+3}=\xi_{j}$, $j\in\{1,\ldots,m\}$.
From \eqref{eq:bs:2}, we have
$B_{1,0}(x\mid\bm{s}_2)=B_{2,0}(x\mid\bm{s}_2)=0$,
$B_{3,0}(x\mid\bm{s}_2)=\bm{1}(t_3\le x<t_4)$, $\ldots$,
$B_{m+3,0}(x\mid\bm{s}_2)=\bm{1}(t_{m+3}\le x<t_{m+4})$, and
$B_{m+4,0}(x\mid\bm{s}_2)=B_{m+5,0}(x\mid\bm{s}_2)=0$.
Then we may obtain $B_{i,1}(x\mid\bm{s}_2)$
for $i\in\{1,\ldots,p+1\}$ by setting $k=1$ in \eqref{eq:bs:1}.
At last, the desired quadratic basis $B_{i,1}(x\mid\bm{s}_2)$
for $i\in\{1,\ldots,p\}$ can be produced by \eqref{eq:bs:1} again with $k=2$.


<!-- some properties of B-splines -->
The B-splines of degree $k$ are nonnegative over $[L,R]$
subject to $\sum_{i=1}^{p} B_{i,k}(x\mid\bm{s}_k)=1$.
In addition, the B-splines have *local support*
meaning that $B_{i,k}(x\mid\bm{s}_k)$ is positive
for $x\in(t_i, t_{i+k+1})$ but zero for $x$ outside of
$[t_i, t_{i+k+1}]$, which is an important property we may utilize
when implementing \eqref{eq:bs:1}.
<!-- introduce derivatives and integrals -->
Although the B-splines are defined recursively, they are still
polynomials in essence.
Therefore, one would expect closed-form expressions of their
derivatives and integrals.
@boor1978practical [Chapter 10] gave the derivatives and integrals of B-splines
in closed-form formulas, which were more recently reviewed and discussed by
@bhatti2006calculation.


<!-- derivatives -->
The first derivative of $i$th B-spline basis of degree $k$,
for $k\in\{1,2,\ldots\}$,
can be derived by induction using \eqref{eq:bs:1} and is given by
\begin{align}\label{eq:dbs:1}
\dv{x} B_{i,k}(x\mid\bm{s}_k) =
\left(\frac{k}{t_{i+k}-t_i}\right) B_{i,k-1}(x\mid\bm{s}_k) -
\left(\frac{k}{t_{i+k+1}-t_{i+1}}\right) B_{i+1,k-1}(x\mid\bm{s}_k),
\end{align}
where $i\in\{1,\ldots,p\}$.
We may derive the second derivatives by taking the derivatives
again on both sides of \eqref{eq:dbs:1}
and applying \eqref{eq:dbs:1} for B-splines of degree $k-1$.
Such a procedure can be repeated for
the derivatives of higher order.


<!-- integrals -->
To derive the integrals of B-splines,
let us consider the first derivative of the following spline function
$s(x) = \sum_{i=1}^{p+1} \beta_i B_{i,k+1}(x\mid\bm{s}_{k+1})$,
\begin{align}
\dv{x}\sum_{i=1}^{p+1} \beta_i B_{i,k+1}(x\mid\bm{s}_{k+1})
& = \sum_{i=1}^{p+1}
\left[\frac{\beta_i(k+1)}{t_{i+k+1}-t_i}\right] B_{i,k}(x\mid\bm{s}_{k+1}) -
\left[\frac{\beta_i(k+1)}{t_{i+k+2}-t_{i+1}}\right]
B_{i+1,k}(x\mid\bm{s}_{k+1})\nonumber\\
& = \sum_{i=2}^{p+1} (k+1)
\left(\frac{\beta_i-\beta_{i-1}}{t_{i+k+1}-t_{i}}\right)
B_{i,k}(x\mid\bm{s}_{k+1})\label{eq:dbs:2},
\end{align}
where $t_l$ is defined for $\bm{s}_{k+1}$,
$l\in\{1,\ldots,d+p+2\}$,
and the second equation holds as we have
$B_{1,k}(x\mid\bm{s}_{k+1})=B_{p+2,k}(x\mid\bm{s}_{k+1})=0$.
Let $\beta_l=\bm{1}(l\ge j)$ for a given $j\in\{2,\ldots,p+1\}$.
Then
\begin{align}\label{eq:dbs:3}
\dv{x} \sum_{l=j}^{p+1} B_{l,k+1}(x\mid\bm{s}_{k+1})
= \left(\frac{k+1}{t_{j+k+1}-t_{j}}\right)B_{j,k}(x\mid\bm{s}_{k+1}).
\end{align}
By integrating the both side of \eqref{eq:dbs:3},
replacing $B_{j,k}(x\mid\bm{s}_{k+1})$ equivalently
with $B_{j-1,k}(x\mid\bm{s}_{k})$, and replacing $j$ with $i+1$,
we obtain the integral of $B_{i,k}(x\mid\bm{s}_{k})$ from $t_1$ to $x$
as follows:
\begin{align}\label{eq:ibs:1}
\int_{t_1}^x B_{i,k}(t\mid\bm{s}_{k})dt =
\left(\frac{t_{i+k+2}-t_{i+1}}{k+1}\right)
\sum_{l=i+1}^{p+1} B_{l,k+1}(x\mid\bm{s}_{k+1}).
\end{align}
where $t_{i+k+2}$ and $t_{i+1}$ are defined for
$\bm{s}_{k+1}$, $i\in\{1,\ldots,p\}$.


## Natural Cubic Splines

<!-- define natural spline function -->

<!-- WW: In fact, a natural spline can be defined for any spline function of odd
degree or even order.  See page 24 of Hastie 1990 GAM book and page 309 of
schumaker2007spline for example.  The general definition is a spline function of
order 2m with order m at and beyond boundary.-->

A cubic spline function $s(x)=\bm{\beta}^{\top}\bm{B}_3(x)$
is called a natural cubic spline function if it is subject to
the additional constraints that
$\eval{\dv*[2]{s(x)}{x}}_{x=L}=\eval{\dv*[2]{s(x)}{x}}_{x=U}=0$,
where $\bm{\beta} = (\beta_1, \ldots, \beta_p)^{\top}$,
$\bm{B}_3(x)=(B_{1,3}(x), \ldots, B_{p,3}(x))^{\top}$
is the cubic spline vector, and $p=m+4$.
In addition, a natural cubic spline function is
linear beyond the boundary, which suggests that
the first (second) derivatives remain constants (zeros)
outside the boundary.
By definition, one can construct a natural cubic spline function
from cubic splines, such as cubic B-splines, and impose
the curvature constraints at and beyond the boundary.
<!-- derive natural cubic splines -->
Alternatively, we show how to derive a set of cubic spline basis functions
that satisfy those curvature conditions.


Let $\bm{B}_3(x)$ represent the cubic B-spline basis vector
for given boundary and $m$ distinct interior knots.
Consider a set of cubic splines $\{N_{i}(x)\mid i\in\{1,\ldots,l\}\}$
satisfying that $\bm{N}(x)=\bm{H}^{\top}\bm{B}_3(x)$, where
$\bm{N}(x)=(N_{1}(x), \ldots, N_{l}(x))^{\top}$ and
$\bm{H}$ is a $p\times l$ full column rank matrix.  Suppose
$\tilde{s}(x)=\tilde{\bm{\beta}}^{\top}\bm{N}(x)=
\tilde{\bm{\beta}}^{\top}\bm{H}^{\top}\bm{B}_3(x)$
is a natural cubic spline function, where
$\tilde{\bm{\beta}}=(\tilde{\beta}_1,\ldots,\tilde{\beta}_{l})^{\top}$.
Let $\dv*[2]{\bm{B}_3(x)}{x}=(\dv*[2]{B_{1,3}(x)}{x}, \ldots,
\dv*[2]{B_{p,3}(x)}{x})^{\top}$ denote the elementwise second derivatives of
$\bm{B}_3(x)$.
Then the curvature conditions at the boundary are equivalent to
$\bm{C}^{\top}\bm{H}\tilde{\bm{\beta}}=\bm{0}$, where
$\bm{C}=(\eval{\dv*[2]{\bm{B}_3(x)}{x}}_{x=L},
\eval{\dv*[2]{\bm{B}_3(x)}{x}}_{x=U})$.
It can be verified from \eqref{eq:dbs:1}
that $\bm{C}$ is of full column rank.
We define $\bm{N}(x)=\bm{H}^{\top} \bm{B}_3(x)$
forms a set of natural cubic splines if
$\bm{C}^{\top}\bm{H}\tilde{\bm{\beta}}=\bm{0}$ holds
$\forall\bm{\beta}\in\mathbb{R}^{p}$.
Therefore, it is desired to find a matrix $\bm{H}$ such that
$\bm{C}^{\top}\bm{H}=\bm{0}$, i.e.,
the columns of $\bm{H}$ belong to the null space of $\bm{C}^{\top}$.


<!-- 1. H from QR decomposition as splines::ns() dose -->
For a $p\times q$ full column rank rectangular matrix $\bm{C}$, where $p>q=2$,
one may obtain a set of orthogonal basis functions for the null space of
$\bm{C}^{\top}$ from the QR decomposition of $\bm{C}$.
More specifically, suppose the QR decomposition of $\bm{C}$ gives
\begin{align*}
\bm{C}=\bm{Q}\bm{R}=[\bm{Q}_1 ~ \bm{Q}_2]
\left[\begin{array}{c}
\bm{R}_1 \\ \bm{0}
\end{array}\right] = \bm{Q}_1\bm{R}_1,
\end{align*}
where $\bm{Q}$ is a $p\times p$ orthogonal matrix,
$\bm{Q}_1 \in \mathbb{R}^{p\times q}$ and
$\bm{Q}_2 \in \mathbb{R}^{p\times (p-q)}$
subject to $\bm{Q}_1^{\top}\bm{Q}_2=\bm{0}$,
$\bm{R}\in\mathbb{R}^{p\times q}$, and $\bm{R}_1\in\mathbb{R}^{q\times q}$.
Then the columns of $\bm{Q}_2$ can serve as the orthogonal basis functions for
the null space of $\bm{C}^{\top}$ as $\bm{C}^{\top} \bm{Q}_2 = \bm{0}$.
Let $\bm{H}=\bm{Q}_2$ and we obtain a set of natural cubic spline bases as
$\bm{N}(x)=\bm{Q}_2^{\top}\bm{B}_3(x)$.
It is the procedure that the function `ns()` of the \pkg{splines}
package follows.

<!-- some discussion at
https://stat.ethz.ch/pipermail/r-help/2003-May/033460.html -->

The approach based on the QR decomposition works for any cubic splines.
However, it does not take advantage of the local support property of B-splines.
We can explicitly write down $\bm{C}$ for cubic
B-splines from \eqref{eq:dbs:1}.
The first and second columns of $\bm{C}$ denoted by
$\bm{C}_1$ and $\bm{C}_2$ are, respectively,
\begin{align*}
\bm{C}_1=\left(C_{1,1}, C_{2,1}, C_{3,1},
\bm{0}_{p-3}^{\top}\right)^{\top},~
\bm{C}_2=\left(\bm{0}_{p-3}^{\top},
C_{p-2,2}, C_{p-1,2}, C_{p,2}\right)^{\top},
\end{align*}
where $C_{1,1}=6[(t_5-t_2)(t_5-t_3)]^{-1}$,
$C_{3,1}=6[(t_6-t_3)(t_5-t_3)]^{-1}$,
$C_{2,1}=-C_{1,1}-C_{3,1}$,
$C_{p-2,2}=6[(t_{m+6}-t_{m+3})(t_{m+6}-t_{m+4})]^{-1}$,
$C_{p,2}=6[(t_{m+7}-t_{m+4})(t_{m+6}-t_{m+4})]^{-1}$,
$C_{p-1,2}=-C_{p-2,2}-C_{p,2}$,
and $\bm{0}_{p-3}$ is a zero vector of length $(p-3)$.
Given the explicit expression of $\bm{C}$ for cubic B-splines
and number of internal knots, we are able to obtain
specific choices of $\bm{H}$ such that $\bm{C}^{\top}\bm{H}=\bm{0}$
without using the QR decomposition.
When no internal knot or one internal knot is placed,
we may choose, respectively,
\begin{align}
\bm{H}^{\top} = \left[\begin{array}{cccc}
    3 & 2 & 1 & 0\\
    0 & 1 & 2 & 3
\end{array}\right],
\end{align}
and
\begin{align}
\bm{H}^{\top} = \left[\begin{array}{ccccc}
    - C_{2,1}/C_{1,1} & 1 & 0 & 0 & 0\\
    0 & - C_{3,1}/C_{2,1} & 1 & - C_{p-2,2}/C_{p-1,2} & 0\\
    0 & 0 & 0 & 1 & - C_{p-1,2}/C_{p,2}
\end{array}\right].
\end{align}
While two or more internal knots are placed, we can let
\begin{align}
\bm{H}^{\top} = \left[\begin{array}{ccccccc}
    1 & 1 & 1 & \bm{0}_{m-2}^{\top} & 0 & 0 & 0\\
    0 & 1 & - C_{2,1}/C_{3,1} & \bm{0}_{m-2}^{\top} & 0 & 0 & 0\\
    \bm{0}_{m-2} & \bm{0}_{m-2} & \bm{0}_{m-2} & \bm{I}_{m-2} &
    \bm{0}_{m-2} & \bm{0}_{m-2} & \bm{0}_{m-2}\\
    0 & 0 & 0 & \bm{0}_{m-2}^{\top} & - C_{p-1,2}/C_{p-2,2} & 1 & 0\\
    0 & 0 & 0 & \bm{0}_{m-2}^{\top} & 1 & 1 & 1
\end{array}\right],
\end{align}
where $\bm{I}_{m-2}$ is a $(m-2)\times (m-2)$ identity matrix and
$\bm{0}_{m-2}$ is a zero vector of length $(m-2)$.
Notice that the chosen $\bm{H}$ consists of nonnegative elements.
The natural cubic splines given by $\bm{N}(x)=\bm{H}^{\top}\bm{B}_3(x)$
are thus nonnegative within the boundary.
Additionally, the derivatives and integrals of the resulting natural cubic
splines can be obtained from the derived $\bm{H}^{\top}$.
For example, the element-wise first derivatives of $\bm{N}(x)$
are given by $\bm{H}^{\top}\dv*{\bm{B}_3(x)}{x}$.
The function `naturalSpline()` in the \pkg{splines2} package
follows this approach to produce natural cubic splines,
their corresponding derivatives, and integrals.


# Micro-Benchmarks

The \proglang{R} code that produced the micro-benchmark results in Section 5 of
the main text is as follows.

```{r benchmark-src, cache = TRUE}
library(microbenchmark)
library(splines)
library(splines2)

set.seed(123)
x <- seq.int(0, 1, 1e-3) # set x
degree <- 3              # cubic basis functions
ord <- degree + 1        # set order
internal_knots <- seq.int(0.1, 0.9, 0.1)      # set internal knots
boundary_knots <- range(x)                    # set boundary knots
## knot sequence without internal knots
all_knots0 <- rep(boundary_knots, each = ord)
## knot sequence with internal knots
all_knots <- sort(c(internal_knots, rep(boundary_knots, ord)))
coef_sp <- rnorm(length(all_knots) - ord) # set coef for comparing ibs::ibs()
derivs <- 2                               # comparing second derivatives

## B-splines
bs_benchmark <- microbenchmark(
    "splines::bs()" = bs(x, knots = internal_knots, degree = degree,
                         intercept = TRUE),
    "splines::splineDesign()" = splineDesign(x, knots = all_knots, ord = ord),
    "splines2::bSpline()" = bSpline(x, knots = internal_knots, degree = degree,
                                    intercept = TRUE),
    times = 1e3
)

## Bernstein Polynomial (B-splines without internal knots)
bp_benchmark <- microbenchmark(
    "splines::bs()" = bs(x, degree = degree, intercept = TRUE),
    "splines::splineDesign()" = splineDesign(x, knots = all_knots0, ord = ord),
    "splines2::bernsteinPoly()" = bernsteinPoly(x, degree = degree,
                                                intercept = TRUE),
    times = 1e3
)

## Second Derivatives of B-splines
dbs_benchmark <- microbenchmark(
    "splines::splineDesign()" = splineDesign(x, knots = all_knots,
                                             ord = ord, derivs = derivs),
    "splines2::dbs()" = dbs(x, derivs = derivs, knots = internal_knots,
                            degree = degree, intercept = TRUE),
    times = 1e3
)

## Integrals of B-splines
ibs_benchmark <- microbenchmark(
    "ibs::ibs()" = ibs::ibs(x, knots = all_knots, ord = ord, coef = coef_sp),
    "splines2::ibs()" = as.numeric(
        splines2::ibs(x, knots = internal_knots, degree = degree,
                      intercept = TRUE) %*% coef_sp
    ),
    times = 1e3
)

## Natural Cubic Splines
ns_benchmark <- microbenchmark(
    "splines::ns()" = ns(x, knots = internal_knots, intercept = TRUE),
    "splines2::naturalSpline()" = naturalSpline(
        x, knots = internal_knots, intercept = TRUE
    ),
    times = 1e3
)

## Periodic Splines
pbs_benchmark <- microbenchmark(
    "pbs::pbs()" = pbs::pbs(x, knots = internal_knots, degree = degree,
                            intercept = TRUE, periodic = TRUE),
    "splines2::mSpline()" = mSpline(
        x, knots = internal_knots, degree = degree,
        intercept = TRUE, periodic = TRUE
    ),
    times = 1e3
)
```

The information of the \proglang{R} session where the micro-benchmarks were
performed is as follows:

```{r sessionInfo, cache = TRUE}
xfun::session_info(package = c("splines", "splines2",
                               "ibs", "pbs", "microbenchmark"))
```

We visualized the relative performance given in Figure 7 of the main text as
follows:

```{r mb-plot, eval = FALSE}
## add labels
bs_benchmark$basis <- "B-Splines"
bp_benchmark$basis <- "Bernstein Polynomials"
dbs_benchmark$basis <- "Derivatives of B-Splines"
ibs_benchmark$basis <- "Integrals of B-Splines"
ns_benchmark$basis <- "Natural Cubic Splines"
pbs_benchmark$basis <- "Periodic Splines"

## relative benchmark by median of splines2
rel_med <- function(dat) {
    idx <- grepl("splines2", dat$expr, fixed = TRUE)
    med_x <- median(dat[idx, "time"])
    dat$time <- dat$time / med_x
    dat
}

## create data for the plot
gg_dat <- rbind(
    rel_med(bs_benchmark),
    rel_med(bp_benchmark),
    rel_med(dbs_benchmark),
    rel_med(ibs_benchmark),
    rel_med(ns_benchmark),
    rel_med(pbs_benchmark)
)
gg_dat <- as.data.frame(gg_dat)

## visualization with the help of ggplot2
library(ggplot2)
ggplot(data = gg_dat, aes(x = expr, y = time)) +
    geom_boxplot(outlier.size = 0.5, alpha = 0.5) +
    coord_flip() +
    facet_wrap(vars(basis), scales = "free", ncol = 2) +
    scale_y_continuous(trans = "log", breaks = c(1, 2, 4, 8, 16),
                       limits = c(0.5, 16)) +
    theme_bw() +
    ylab("Relative Performance Compared to splines2") +
    xlab("")
```
