# Manusrcipt

## Usage

We used the R package *renv* to manage the R package dependency.
The package library for this project needs to be initialized at first
by running the following command in a bash shell:

```bash
Rscript -e "renv::restore()"
```

Alternatively, we may launch an R session under this directory and running
`renv::restore()` in the R console.  The package *renv* will try to bootstrap
itself and restore all the R packages listed in `../renv.lock`.

Once the package library has been restored, we may render the source documents
written in R Markdown to PDFs with the help of `Makefile` as follows:

```bash
make
```

RStudio users should be able to knit the source documents by the `knit` button
available in RStudio interface as well.
