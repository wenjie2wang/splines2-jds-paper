# Shape-Restricted Regression Splines with R Package *splines2*

This repository was set up for authoring the manuscript titled "shape-restricted
regression splines with R package *splines2*".
See the source documents under the directory `manuscript/` for details.
